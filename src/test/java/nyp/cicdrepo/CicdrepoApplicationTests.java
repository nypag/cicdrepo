package nyp.cicdrepo;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CicdrepoApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void TestCalculateSum() {
		int result = 3;
		int num1 = 1;
		int num2 = 2;
		assertEquals(num1+num2, result);
	}

}
