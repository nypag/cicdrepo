package nyp.cicdrepo;

import java.util.Scanner;

public class CicdrepoApplication {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the first number you want to sum up:");
        int num1 = in.nextInt();
        System.out.println("Second number:");
        int num2 = in.nextInt();
        int result = calculateSum(num1, num2);
        System.out.println("Result: " + System.lineSeparator() + result);
        in.close();
	}
	
	public static int calculateSum(int num1, int num2) {
		return num1+num2;
	}

}
